# Solnet CMS Developer practical assignment - Senior level 

Install Vagrant and VirtualBox.

https://www.vagrantup.com/downloads.html
https://www.virtualbox.org/wiki/Downloads

In a terminal, in the same folder as this README.md file, run the command:

```
vagrant up
```

The VM image will download, and once it has started will be running the example starter code.

You can now see the example starter project at http://192.168.33.10/ - note that the first load
will run SilverStripe's setup procedure, which may take a few seconds.

