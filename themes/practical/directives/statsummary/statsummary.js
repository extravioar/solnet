var module = angular.module('snDash.home');

module.directive('statSummary', function() {
    return {
        restrict: 'E',
        templateUrl: 'themes/practical/directives/statsummary/statsummary.html',
        scope: true
    };
});