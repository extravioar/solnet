var module = angular.module('snDash.home');

module.directive('projectList', function() {
    return {
        restrict: 'E',
        templateUrl: 'themes/practical/directives/projectlist/projectlist.html',
        scope: true
    };
});