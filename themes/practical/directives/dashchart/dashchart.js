var module = angular.module('snDash.home');

module.directive('dashChart', function() {
    return {
        restrict: 'E',
        templateUrl: 'themes/practical/directives/dashchart/dashchart.html',
        scope: true
    };
});
