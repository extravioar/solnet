<?php

/**
 * Returns data to display on the dashboard
 *
 */
class Dash_Controller extends Controller
{
    /**
     * @var array
     */
    private static $allowed_actions = [
        'getWorkingHours'
    ];

    /**
     * Return array with actual and estimated hours worked per day
     *
     * @param SS_HTTPRequest $request
     * @return array
     */
    public function getWorkingHours(SS_HTTPRequest $request)
    {
        $isAjax = Director::is_ajax();
        return 'test';
        
    }

}
